const hdWalletProvider = require('truffle-hdwallet-provider');
const Web3 = require('web3');
const compiledFactory = require('./build/CampaignFactory.json');
require('dotenv').config();

const provider = new hdWalletProvider(
  process.env.MNEMONIC,
  process.env.GATEWAY
);

const web3 = new Web3(provider);


const deploy = async () => {

  const accounts = await web3.eth.getAccounts();
  
  const result = await new web3.eth.Contract(compiledFactory.abi)
     .deploy({ data: compiledFactory.evm.bytecode.object }) // add 0x bytecode
     .send({ from: accounts[0] }); // remove 'gas'

  console.log('contract deploy to ', result.options.address);
};

deploy();