import web3 from './web3';
import CampaignFactory from './build/CampaignFactory.json';
require('dotenv').config();

const contractInstance = new web3.eth.Contract(
  CampaignFactory.abi, 
  process.env.CONTRACT_ADDRESS
);

export default contractInstance;