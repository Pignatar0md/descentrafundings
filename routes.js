//this file work tied to server.js
// this file is implemented cause next.js doesn't support routing with dynamic arguments like /campaigns/0x3j2h11h2...
//where 0x3j2h11h2... changes. See "next-routes" in google.
const routes = require('next-routes')();// () means that it returns a function

routes
  .add('/campaigns/new', '/campaigns/new')
  .add('/campaigns/:address', '/campaigns/show')
  .add('/campaigns/:address/requests', '/campaigns/requests/index')
  .add('/campaigns/:address/requests/new', '/campaigns/requests/new');

module.exports = routes;