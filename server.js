//this file work tied to routes.js
const { createServer } = require('http');
require('dotenv').config();
const next = require('next');

const app = next({
  dev: process.env.NODE_ENV !== 'production'
});

const routes = require('./routes');
const handler = routes.getRequestHandler(app);

app.prepare().then(() => {
  createServer(handler).listen(process.env.PORT, (err) => {
    if (err) throw err;
    console.log('ready on localhost:', process.env.PORT);
  });
});