import React from 'react';
import { Form, Input, Alert, Button } from 'antd';
import { Router } from '../routes';
import web3 from '../ethereum/web3';
import Campaign from '../ethereum/campaign';

class ContributeForm extends React.Component {

  state = {
    loading: false,
    errorMessage: '',
    value: ''
  }

  onSubmit = async () => {

    this.setState({ loading: true, errorMessage: '' });
    const campaign = Campaign(this.props.address);//this address comes from show.js
    try {
      const accounts = await web3.eth.getAccounts();
      await campaign.methods.contribute().send({
        from: accounts[0],
        value: web3.utils.toWei(this.state.value, 'ether')
      });
      Router.replaceRoute(`/campaigns/${this.props.address}`);//reloads the page
    } catch (err) {
      this.setState({ errorMessage: err.message });
    }
    this.setState({ loading: false, value: '' });
  }

  render() {
    const { errorMessage, loading } = this.state;
    return (
      <Form onFinish={this.onSubmit}>
        <h4>Amount to contribute</h4>
        <Form.Item
          rules={[{ message: 'Please input your contribution in eth' }]}
        >
          <Input
            value={this.state.value}
            onChange={event => this.setState({ value: event.target.value })}
            placeholder="eth"
          />
        </Form.Item>
        <Alert
          style={{ display: errorMessage !== '' ? 'block' : 'none', bottom: '12px' }}
          message={errorMessage} type="error"
        />

        <Form.Item>
          <Button type="primary" htmlType="submit" loading={loading}>
            Contribute
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

export default ContributeForm;