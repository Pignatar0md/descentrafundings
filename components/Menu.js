import React from 'react';
import { Menu } from 'antd';
import { NodeIndexOutlined, AppstoreOutlined } from '@ant-design/icons';
import { Link } from '../routes';

export default () => {
  return (
    <Menu theme="dark" mode="horizontal">
      <Menu.Item key="mail" icon={<NodeIndexOutlined />}>
        <Link route="/">
          <a>DescentraFunding</a>
        </Link>
        </Menu.Item>
        <Menu.Item key="add" style={{float: 'right'}}>
        <Link route="/campaigns/new" style={{float: 'right'}}>
          <a>+</a>
        </Link>
        </Menu.Item>
        <Menu.Item key="app" icon={<AppstoreOutlined />} style={{float: 'right'}}>

        <Link route="/">
          <a>Campaigns</a>
        </Link>
        </Menu.Item>
    </Menu>
  );
};