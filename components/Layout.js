import React from 'react';
import Menu from './Menu';
import { Layout } from 'antd';
const { Header, Content } = Layout;

export default ({ children }) => {
  return (
    <Layout>
      <Header>
        <Menu />
      </Header>
      <Content style={{ padding: '10px 65px' }}>
        {children}
      </Content>
    </Layout>
  );
};