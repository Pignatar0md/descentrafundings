import React from 'react';
import { Card, Col, Row, Button } from 'antd';
import { Link } from '../../routes';
import Layout from '../../components/Layout';
import Campaign from '../../ethereum/campaign';
import web3 from '../../ethereum/web3';
import ContributeForm from '../../components/ContributeForm';

const { Meta } = Card;

class CampaignShow extends React.Component {

  static async getInitialProps(props) {
    const campaign = Campaign(props.query.address);//get the address from the url
    const summary = await campaign.methods.getSummary().call();
    return {
      minContribution: summary[0],
      balance: summary[1],
      requestsCount: summary[2],
      approversCount: summary[3],
      managerAddress: summary[4],
      address: props.query.address
    };
  }

  renderCards() {
    const {
      minContribution,
      balance,
      requestsCount,
      approversCount,
      managerAddress
    } = this.props;

    return (
      <div className="site-card-wrapper">
        <Row>
          <Col>
            <Card
              hoverable
              style={{ width: 460, margin: '10px 10px 10px 0px' }}
            >
              <Meta 
                title={managerAddress} 
                description="Address of manager" 
              />
              <br />
              <p>
                The manager created this campaign and can create requests to withdraw money.
              </p>
            </Card>
          </Col>
          <Col>
            <Card
              hoverable
              style={{ width: 460, margin: '10px 10px 10px 10px' }}
            >
              <Meta 
                title={minContribution} 
                description="Minimum contribution(wei)" 
              />
              <br />
              <p>
                You must contribute at least this much wei to become an approver.
              </p>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col>
            <Card
              hoverable
              style={{ width: 460, margin: '10px 10px 10px 0px' }}
            >
              <Meta
                title={requestsCount} 
                description="Number of requests" 
              />
              <br />
              <p>
                A request tries to withdraw money from the contract. 
                Requests must be approved by approvers.
              </p>
            </Card>
          </Col>
          <Col>
            <Card
              hoverable
              style={{ width: 460, margin: '10px 10px 10px 10px' }}
            >
              <Meta 
                title={approversCount} 
                description="Number of approvers"
              />
              <br />
              <p>
                Number of people who have donated to this campaign.
              </p>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col>
            <Card
              hoverable
              style={{ width: 460, margin: '10px 10px 10px 0px' }}
            >
              <Meta
                title={web3.utils.fromWei(balance, 'ether')}
                description="Campaign alance (eth)"
              />
              <br />
              <p>
                The balance is how much money this campaign has left to spend.
              </p>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }

  render() {
    return (
      <Layout>
        <>
          <h3>Campaign Info</h3>
          <Row>
            <Col>
              {this.renderCards()}
              <Link route={`/campaigns/${this.props.address}/requests`}>
                <Button>
                  View Requests
                </Button>
              </Link>
            </Col>
            <Col span={4} offset={5}>
              <ContributeForm address={this.props.address} />
            </Col>
          </Row>
        </>
      </Layout>
    );
  }
}

export default CampaignShow;