import React from 'react';
import { Form, Button, Input, Alert } from 'antd';
import { Link, Router } from '../../../routes';
import web3 from '../../../ethereum/web3';
import Campaign from '../../../ethereum/campaign';
import Layout from '../../../components/Layout';

class RequestNew extends React.Component {

  state = {
    value: '',
    description: '',
    recipient: '',
    loading: false,
    errorMessage: ''
  }

  static async getInitialProps(props) {
    const { address } = props.query;

    return { address };
  }

  onSubmit = async () => {

    const campaign = Campaign(this.props.address);
    const { description, value, recipient } = this.state;
    this.setState({ loading: true, errorMessage: '' });
    try {
      const accounts = await web3.eth.getAccounts();
      await campaign.methods.createRequest(description, web3.utils.toWei(value, 'ether'), recipient)
        .send({ from: accounts[0] });
      Router.pushRoute(`/campaigns/${this.props.address}/requests`)
    } catch (err) {
      this.setState({ errorMessage: err.message });
    }
    this.setState({ loading: false });
  }

  render() {
    const { errorMessage, loading, value, description } = this.state;
    return (
      <Layout>
        <Link route={`/campaigns/${this.props.address}/requests`}>
          <a>Back</a>
        </Link>
        <h2>Create a request</h2>
        <Form onFinish={this.onSubmit}>
          <Form.Item>
            <label>Description</label>
            <Input
              value={description}
              onChange={event => this.setState({ description: event.target.value })}
            />
          </Form.Item>
          <Form.Item>
            <label>Value in Eth</label>
            <Input
              value={value}
              onChange={event => this.setState({ value: event.target.value })}
            />
          </Form.Item>
          <Form.Item>
            <label>Recipient</label>
            <Input
              value={this.state.recipient}
              onChange={event => this.setState({ recipient: event.target.value })}
              />
          </Form.Item>
          <Alert
            style={{ display: errorMessage !== '' ? 'block' : 'none', bottom: '12px' }}
            message={errorMessage} type="error"
          />
          <Form.Item>
            <Button type="primary" htmlType="submit" loading={loading}>
              Create!
            </Button>
          </Form.Item>
        </Form>
      </Layout>
      );
  }
}

export default RequestNew;