import React from 'react';
import { Button, Table } from 'antd';
import web3 from '../../../ethereum/web3';
import { Router, Link } from '../../../routes';
import Layout from '../../../components/Layout';
import Campaign from '../../../ethereum/campaign';

class RequestIndex extends React.Component {

  static async getInitialProps({ query }) {
    const { address } = query;
    const campaign = Campaign(address);
    const requestCount = await campaign.methods.getRequestsCount().call();
    const approversCount = await campaign.methods.approversCount().call();

    const requests = await Promise.all(
      Array(parseInt(requestCount)).fill().map((elem, index) => {
        return campaign.methods.requests(index).call();
      })
    );
    return { address, requests, requestCount, approversCount };
  }

  onApprove = async id => {
    const accounts = await web3.eth.getAccounts();
    const campaign = Campaign(this.props.address);
    await campaign.methods.approveRequest(id).send({
      from: accounts[0]
    });
    Router.replaceRoute(`/campaigns/${this.props.address}/requests`);
  }

  onFinalize = async id => {
    const accounts = await web3.eth.getAccounts();
    const campaign = Campaign(this.props.address);
    await campaign.methods.finalizeRequest(id).send({
      from: accounts[0]
    });
  }

  renderRows() {
    const { requests, approversCount } = this.props;
    let data = [];
    requests.map((request, index) => {
        const { description, value, recipient, approvalCount, complete } = request;
        data.push({
          id: index,
          description,
          amount: web3.utils.fromWei(value, 'ether'),
          recipient,
          approvalCount: approvalCount + '/' + approversCount,
          approve: !complete && <Button
            style={{ color: "green", borderColor: "green" }} 
            onClick={() => this.onApprove(index)}>
              Approve
          </Button>,
          finalize: !complete && <Button
            onClick={() => this.onFinalize(index)}
            style={{ color: "#04B4AE", borderColor: "#04B4AE" }} >
              Finalize
          </Button>
        });
    });
    return data;
  }

  render() {
    const { Column } = Table;
    const { address, requestCount } = this.props;
    return (
      <Layout>
        <h3>Request list</h3>
        <Link route={`/campaigns/${address}/requests/new`}>
          <a>
            <Button type="primary">
              Add Request
            </Button>
          </a>
        </Link>
        <Table
          style={{ marginTop: '20px' }}
          dataSource={this.renderRows()}
        >
          <Column title="ID" dataIndex="id" key="id" />
          <Column title="Description" dataIndex="description" key="description" />
          <Column title="Amount (eth)" dataIndex="amount" key="amount" />
          <Column title="Recipient" dataIndex="recipient" key="recipient" />
          <Column title="Approval Count" dataIndex="approvalCount" key="approvalCount" />
          <Column title="Approve" dataIndex="approve" key="approve" />
          <Column title="Finalize" dataIndex="finalize" key="finalize" />
        </Table>
        <div>Found { requestCount } requests</div>
      </Layout>
    );
  }
}

export default RequestIndex;