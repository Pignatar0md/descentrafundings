import React from 'react';
import web3 from '../../ethereum/web3';
import factory from '../../ethereum/factory';
import Layout from '../../components/Layout';
import { Form, Input, Button, Alert } from 'antd';
import { Router } from '../../routes';

class CampaignNew extends React.Component {

  state = {
    minimumContribution: '',
    errorMessage: '',
    loading: false
  };

  onSubmit = async () => {

    this.setState({ loading: true, errorMessage: '' });
    try {
      const accounts = await web3.eth.getAccounts();
      await factory.methods
        .createCampaign(this.state.minimumContribution)
        .send({ from: accounts[0] });

      Router.pushRoute('/');
    } catch (err) {
      this.setState({ errorMessage: err.message });
    }
    this.setState({ loading: false });
  };

  render() {
    const { errorMessage, loading, minimumContribution } = this.state;
    return (
      <Layout>
        <h2>Create Campaign</h2>
          <Form onFinish={this.onSubmit}>
            
            <Form.Item
              label="Minimum contribution"
              name="username"
              rules={[{ message: 'Please input your contribution in wei' }]}
            >
              <Input
                value={minimumContribution}
                onChange={event => this.setState({ minimumContribution: event.target.value })}
                placeholder="wei"
              />
            </Form.Item>

            <Alert
              style={{ display: errorMessage !== '' ? 'block' : 'none', bottom: '12px' }}
              message={errorMessage} type="error"
            />

            <Form.Item>
              <Button type="primary" htmlType="submit" loading={loading}>
                Create!
              </Button>
            </Form.Item>

          </Form>
      </Layout>
    );
  }
}

export default CampaignNew;