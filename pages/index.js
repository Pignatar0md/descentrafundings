import React from 'react';
import { Card, Button } from 'antd';
import { PlusCircleOutlined } from '@ant-design/icons';
import factory from '../ethereum/factory';
import Layout from '../components/Layout';
import { Link } from '../routes';

class CampaignIndex extends React.Component {

  static async getInitialProps() {
    const deployedCampaigns = await factory.methods.getDeployedCampaigns().call();
    return { deployedCampaigns };
  }

  renderCampaigns() {
    return this.props.deployedCampaigns.map(address => 
      (
        <Card size="small" style={{ width: 400 }} title={address}>
          <Link route={`/campaigns/${address}`}>
            <a>View Campaign</a>
          </Link>
        </Card>
      )
    );
  }

  render() {    
    return (
      <Layout>
        <div>
          <h2>Open Campaigns</h2>
          <Link route="/campaigns/new">
            <a>
              <Button style={{ float: 'right' }} type="primary" icon={<PlusCircleOutlined />}>
                Create Campaign
              </Button>
            </a>
          </Link>
          {this.renderCampaigns()}
        </div>
      </Layout>
    );
  }
}

export default CampaignIndex;